//
// Created by vivraan on 26-11-2023.
//

#ifndef MISC_H
#define MISC_H

#include <chrono>
#include <cstdint>

using f64 = double;
using u64 = uint64_t;
using i64 = int64_t;
using usize = decltype(sizeof(0));

enum class AngleUnit {
  Deg,
  Rad
};

constexpr f64 sq(const f64 x) noexcept {
  return x * x;
}

auto to_ms(auto x) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(x);
}

#endif //MISC_H
