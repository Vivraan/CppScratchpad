//
// Created by vivraan on 23-11-2023.
//

#include "json_input_gen.h"

#include <algorithm>
#include <charconv>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <execution>
#include <filesystem>
#include <format>
#include <fstream>
#include <mutex>
#include <numbers>
#include <numeric>
#include <print>
#include <random>
#include <string>
#include <system_error>
#include <thread>
#include <vector>

#include "args.h"
#include "haversine.h"
#include "rdtsc.h"
#include "thread_pool.h"

namespace fs = std::filesystem;

LatLon make_random_latlon_rad(RandomStateM1P1& rs) noexcept {
  constexpr auto pi = std::numbers::pi_v<f64>;
  const auto u1 = rs.next();
  const auto u2 = rs.next();
  return {
    // Gives a result between -pi/2 and pi/2 as one would expect for a latitude,
    // in constrast to using acos as one would for spherical coordinates
    .lat = std::asin(u1),
    .lon = pi * u2 // Gives a result between -pi and pi
  };
}

LatLon make_random_latlon_deg(RandomStateM1P1& rs) noexcept {
  constexpr auto inv_pi = std::numbers::inv_pi_v<f64>;
  const auto u1 = rs.next();
  const auto u2 = rs.next();
  return {
    // Gives a result between -90 and 90 as one would expect for a latitude,
    // in constrast to using acos as one would for spherical coordinates
    .lat = std::asin(u1) * inv_pi * 180,
    .lon = 180 * u2 // Gives a result between -180 and 180
  };
}

constexpr AngleUnit parse_deg_or_rad_pref(const Args& args, const AngleUnit default_pref) noexcept {
  const bool use_deg = std::ranges::find(args, "--deg") != args.end();
  const bool use_rad = std::ranges::find(args, "--rad") != args.end();
  if (use_deg && use_rad) {
    std::println(stderr, "[Args Parse Error] Can't use both --deg and --rad.");
    std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
  }
  if (use_deg) {
    return AngleUnit::Deg;
  }
  if (use_rad) {
    return AngleUnit::Rad;
  }
  return default_pref;
}

constexpr u64 parse_u64(const std::string_view sv) noexcept {
  u64 num; // NOLINT(*-init-variables)
  const auto* begin = sv.data();
  const auto* end = sv.data() + sv.size();
  if (auto [chk, ec] = std::from_chars(begin, end, num, 10); ec == std::errc{} && chk == end) {
    return num;
  }
  std::println(stderr, "[Args Parse Error] Error reading number.");
  std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main() indirectly
}

constexpr u64 parse_num_pairs(const Args& args, const u64 default_num_pairs) noexcept {
  const auto it = std::ranges::find(args, "--num-pairs");
  if (it == args.end()) {
    return default_num_pairs;
  }
  const auto itp1 = std::ranges::next(it);
  if (itp1 == args.end()) {
    std::println(stderr, "[Args Parse Error] --num-pairs must be followed by a number.");
    std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
  }
  const u64 num_pairs = parse_u64(*itp1);
  return num_pairs;
}

constexpr u64 parse_rand_seed(const Args& args, const u64 default_seed) noexcept {
  const auto it = std::ranges::find(args, "--seed");
  if (it == args.end()) {
    return default_seed;
  }
  const auto itp1 = std::ranges::next(it);
  if (itp1 == args.end()) {
    std::println(stderr, "[Args Parse Error] --seed must be followed by a number.");
    std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
  }
  const u64 seed = parse_u64(*itp1);
  return seed;
}

constexpr fs::path parse_output_path(const Args& args, const fs::path& default_path) noexcept {
  const auto it = std::ranges::find(args, "--output");
  if (it == args.end()) {
    return default_path;
  }
  const auto itp1 = std::ranges::next(it);
  if (itp1 == args.end()) {
    std::println(stderr, "[Args Parse Error] --output must be followed by a file name.");
    std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
  }
  return fs::path{*itp1};
}

void generate(const Args& args) {
  using namespace std::string_view_literals;

  const AngleUnit pref = parse_deg_or_rad_pref(args, AngleUnit::Rad);
  // ReSharper disable once CppLocalVariableMayBeConst -- this is a function pointer!
  auto latlon_rand_func = pref == AngleUnit::Deg ? make_random_latlon_deg : make_random_latlon_rad;
  auto haversine = pref == AngleUnit::Deg ? haversine_deg : haversine_rad;

  const auto num_pairs = parse_num_pairs(args, 100);
  const auto seed = parse_rand_seed(args, std::mt19937_64::default_seed);
  const auto output_path = parse_output_path(args, fs::current_path().parent_path() / "data");

  const auto num_threads = std::thread::hardware_concurrency();
  const auto num_resources = num_threads;
  std::vector<std::string> json_input_parts(num_resources, "");
  std::vector<std::vector<f64>> haversine_results_parts(num_resources);
  std::vector<std::mutex> json_input_mutexes(num_resources);

  const auto pair_gen_start = read_cpu_timer();
  /* block begin */ {
    constexpr auto chunk_sz = 1024ULL;
    ThreadPool thread_pool{num_threads};
    for (u64 i = 0; i < 2 * num_pairs; i += chunk_sz) {
      thread_pool.enqueue([&, i, chunk_sz] {
        RandomStateM1P1 random_state{seed + i};

        constexpr auto buf_sz = 32ULL;
        using CharBuf = std::array<char, buf_sz>;
        std::array<CharBuf, 2 * chunk_sz> bufs{};
        std::array<f64, 2 * chunk_sz> raw_latlons{};

        // Write coords to small charbufs on the stack
        const auto n = 2 * (i + chunk_sz > 2 * num_pairs ? num_pairs % chunk_sz : chunk_sz);
        for (u64 j = 0; j < n; j += 2) {
          const auto [lat, lon] = latlon_rand_func(random_state);
          raw_latlons[j] = lat;
          raw_latlons[j + 1] = lon;
          auto* p0b = bufs[j].data();
          auto* p1b = bufs[j + 1].data();
          auto* p0e = p0b + buf_sz;
          auto* p1e = p1b + buf_sz;
          const auto [lat_sz, lat_ec] = std::to_chars(p0b, p0e, lat, std::chars_format::fixed);
          // ReSharper disable once CppTooWideScopeInitStatement
          const auto [lon_sz, lon_ec] = std::to_chars(p1b, p1e, lon);
          if (lat_ec != std::errc{} || lon_ec != std::errc{}) {
            std::println(stderr, "[Generation Error] Error writing number.");
            std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
          }
        }

        // Write the charbufs into string accumulators, called parts here
        for (u64 t = 0; t < num_resources; t++) {
          const std::unique_lock lock{json_input_mutexes[t], std::try_to_lock};
          if (!lock.owns_lock()) {
            if (t == num_resources - 1) {
              t = -1;
            }
            continue;
          }
          auto& input_part = json_input_parts[t];
          auto& hvres_part = haversine_results_parts[t];
          for (u64 j = 0; j < n; j += 4) {
            hvres_part.push_back(haversine(
              raw_latlons[j],
              raw_latlons[j + 1],
              raw_latlons[j + 2],
              raw_latlons[j + 3],
              EARTH_RADIUS_KM
            ));
            input_part += R"(    {"p1": )";
            input_part += bufs[j].data();
            input_part += R"(, "l1": )";
            input_part += bufs[j + 1].data();
            input_part += R"(, "p2": )";
            input_part += bufs[j + 2].data();
            input_part += R"(, "l2": )";
            input_part += bufs[j + 3].data();
            input_part += "},\n";
          }
          break;
        }
      });
    }
  } /* block end */
  // Remove the last comma and newline
  {
    auto& last_json_part =
      std::ranges::find_last_if(json_input_parts, [](const auto& part) { return !part.empty(); }).front();
    last_json_part.erase(last_json_part.size() - 2);
  }
  const auto pair_gen_elapsed = read_cpu_timer() - pair_gen_start;
  std::println("Generated {:L} pairs in {:L} clocks", num_pairs, pair_gen_elapsed);

  const auto write_to_files_start = read_cpu_timer();
  auto json_size = 0ULL;
  for (const auto& part : json_input_parts) {
    json_size += part.size();
  }
  auto hvres_len = 0ULL;
  for (const auto& part : haversine_results_parts) {
    hvres_len += part.size();
  }
  const auto unit = latlon_rand_func == make_random_latlon_deg ? "deg" : "rad";
  create_directories(output_path);
  const fs::path full_json_path{output_path / std::format("haversine_input_{}_{}.json", num_pairs, unit)};
  const fs::path full_hvres_path{output_path / std::format("haversine_results_{}_{}.bin", num_pairs, unit)};
  const auto json_file_name = full_json_path.filename().string();
  const auto hvres_file_name = full_hvres_path.filename().string();
  std::println("Writing to path {} and {}", full_json_path.string(), full_hvres_path.string());
  /* block begin */ {
    ThreadPool thread_pool{2};
    thread_pool.enqueue([&] {
      std::ofstream hvres_file{full_hvres_path, std::ios::binary};
      if (!hvres_file) {
        std::println(stderr, "[File Error] Couldn't open file {} for writing.", hvres_file_name);
        std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
      }
      std::vector<f64> haversine_results;
      haversine_results.reserve(hvres_len);
      for (const auto& part : haversine_results_parts) {
        haversine_results.insert(haversine_results.end(), part.begin(), part.end());
      }
      hvres_file.write(
        reinterpret_cast<const char*>(haversine_results.data()),
        haversine_results.size() * sizeof(f64)
      );
    });
    thread_pool.enqueue([&] {
      std::ofstream json_file{full_json_path};
      if (!json_file) {
        std::println(stderr, "[File Error] Couldn't open file {} for writing.", json_file_name);
        std::exit(1); // NOLINT(*-mt-unsafe) -- only called from main()
      }
      constexpr auto header = "{\n  \"pairs\": [\n"sv;
      constexpr auto footer = "\n  ]\n}"sv;
      std::string json_object;
      json_object.reserve(header.size() + json_size + footer.size());
      json_object += header;
      for (const auto& part : json_input_parts) {
        json_object += part;
      }
      json_object += footer;
      json_file << json_object;
    });
  } /* block end */
  const auto write_to_file_elapsed = read_cpu_timer() - write_to_files_start;

  std::println(R"(Wrote {:L} bytes to {} and {:L} bytes to {} in {:L} clocks
Total time elapsed: {:L} clocks)",
    json_size, json_file_name, hvres_len * sizeof(f64), hvres_file_name,
    write_to_file_elapsed, pair_gen_elapsed + write_to_file_elapsed);

  // Casey's style
  f64 sum = 0.0;
  for (const auto& part : haversine_results_parts) {
    sum += std::reduce(std::execution::par_unseq, part.begin(), part.end(), 0.0);
  }
  std::println("Sum of all haversine results: {:Lf}", sum);
}
