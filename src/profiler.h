//
// Created by vivraan on 27-11-2023.
//

#ifndef PROFILER_H
#define PROFILER_H

#ifndef FULL_PROFILER
#define FULL_PROFILER 0
#endif

#include "rdtsc.h"

static u64 nanos(const u64 tsc, const u64 cpu_freq) {
  return static_cast<u64>(std::round(1e9 * tsc / cpu_freq));
}

#if FULL_PROFILER

#include <print>
#include <gsl/gsl>

struct ProfileAnchor {
  u64 elapsed_tsc_exclusive; // NOTE: DOES NOT include children
  u64 elapsed_tsc_inclusive; // NOTE: DOES include children
  u64 num_hits;
  u64 bytes_processed;
  std::string_view label;
};

struct Profiler {
  enum class AutoPrintSummary : bool {
    No = false,
    Yes = true
  };

  explicit Profiler(const AutoPrintSummary should_print_summary_on_destruction = AutoPrintSummary::No) noexcept
    : should_print_summary_on_destruction{should_print_summary_on_destruction}, start_tsc{read_cpu_timer()},
      cpu_freq{estimate_cpu_timer_frequency()} {}

  ~Profiler() {
    if (should_print_summary_on_destruction == AutoPrintSummary::Yes) {
      print_summary();
    }
  }

  static Profiler G;
  static gsl::index GParentIndex;

  void increment_anchor_hits(const gsl::index index) noexcept {
    anchors[index].num_hits++;
  }

  void print_summary() noexcept;

  std::array<ProfileAnchor, 4096> anchors{};
  AutoPrintSummary should_print_summary_on_destruction;
  u64 start_tsc = 0;
  u64 end_tsc = 0;
  u64 cpu_freq = 0;
};

Profiler Profiler::G{AutoPrintSummary::Yes};
gsl::index Profiler::GParentIndex = 0;

struct ProfileBlock {
  ProfileBlock(const std::string_view label, const gsl::index self_index, const u64 bytes_processed) noexcept
    : label{label}, self_index{self_index} {
    parent_index = Profiler::GParentIndex;

    // ReSharper disable once CppUseStructuredBinding
    auto& self = Profiler::G.anchors[self_index];
    old_tsc_elapsed_inclusive = self.elapsed_tsc_inclusive;
    self.bytes_processed += bytes_processed;

    Profiler::GParentIndex = self_index;
    start_tsc = read_cpu_timer();
  }

  ~ProfileBlock() noexcept {
    const u64 elapsed = read_cpu_timer() - start_tsc;
    Profiler::GParentIndex = parent_index;

    // ReSharper disable CppUseStructuredBinding
    auto& parent = Profiler::G.anchors[parent_index];
    auto& self = Profiler::G.anchors[self_index];
    // ReSharper restore CppUseStructuredBinding

    parent.elapsed_tsc_exclusive -= elapsed;
    self.elapsed_tsc_exclusive += elapsed;
    self.elapsed_tsc_inclusive = old_tsc_elapsed_inclusive + elapsed;
    ++self.num_hits;

    if (self.label.empty()) {
      self.label = label;
    }
  }

  gsl::index get_self_index() const {
    return self_index;
  }

  ProfileBlock(const ProfileBlock&) = delete;
  ProfileBlock(ProfileBlock&&) = delete;
  ProfileBlock& operator=(const ProfileBlock&) = delete;
  ProfileBlock& operator=(ProfileBlock&&) = delete;

  std::string_view label;
  u64 old_tsc_elapsed_inclusive;
  u64 start_tsc;
  gsl::index parent_index;
  gsl::index self_index;
};

using namespace std::string_view_literals;

inline void Profiler::print_summary() noexcept {
  end_tsc = read_cpu_timer();
  u64 total_cpu_elapsed = end_tsc - start_tsc;

  constexpr auto ruler_size = 97;
  std::println("{:=^{}}", "[ Timings ]", ruler_size);
  namespace fs = std::filesystem;
  const fs::path output_path{fs::current_path().parent_path() / "data" / "sample_output.csv"};
  std::ofstream out{output_path, std::ios::trunc};
  if (!out.is_open()) {
    std::println(stderr, "Failed to open log file: {}", std::make_error_condition(std::errc{errno}).message());
    std::exit(1);
  }
  std::println(out, "sep=|");
  auto to_print = std::format(
    "{}|{}|{}|{}|{}|{}|{}",
    "NAME", "~CLOCKS", "~NANOSECONDS", "EXCL %", "INCL %", "MiB PROCESSED", "GiB/s"
  );
  std::println("{}", to_print);
  std::println(out, "{}", to_print);
  u64 net_tsc = 0;
  for (const auto& [elapsed_tsc_exclusive, elapsed_tsc_inclusive, num_hits, bytes_processed, label] : anchors) {
    if (elapsed_tsc_inclusive <= 0) {
      continue;
    }

    const auto name_column = std::string{label} + (num_hits > 1 ? std::format(" (x{:L})", num_hits) : "");
    const auto nanos_inclusive = nanos(elapsed_tsc_inclusive, cpu_freq);
    const auto mib_processed = bytes_processed / (1024.0 * 1024.0);
    const auto gib_per_sec = static_cast<f64>(bytes_processed) / nanos_inclusive;

    to_print = std::format(
      "{}|{}|{}|{:3.2f}|{:3.2f}|{}|{}",
      name_column, elapsed_tsc_exclusive, nanos_inclusive,
      100.0 * elapsed_tsc_exclusive / total_cpu_elapsed,
      100.0 * elapsed_tsc_inclusive / total_cpu_elapsed, mib_processed, gib_per_sec
    );
    std::println("{}", to_print);
    std::println(out, "{}", to_print);
    net_tsc += elapsed_tsc_exclusive;
  }

  auto percent_elapsed_column_net = std::format("{:3.2f}%", 100.0 * net_tsc / total_cpu_elapsed);
  auto percent_elapsed_column_total = std::format("{:3.2f}%", 100.0);
  to_print = std::format(
    R"({}|{}|{}|{}
{}|{}|{}|{}
)",
    "NET ELAPSED", net_tsc, nanos(net_tsc, cpu_freq), percent_elapsed_column_net,
    "TOTAL ELAPSED", total_cpu_elapsed, nanos(total_cpu_elapsed, cpu_freq), percent_elapsed_column_total);
  std::println("{}", to_print);
  std::println(out, "{}", to_print);
  should_print_summary_on_destruction = AutoPrintSummary::No;
}

#define NameConcat2(A, B) A##B
#define NameConcat(A, B) NameConcat2(A, B)
#define TimeThroughput(Name, BytesProcessed) \
	const ProfileBlock NameConcat(Block, __LINE__){(Name), (__COUNTER__ + 1), (BytesProcessed)};
#define TimeBlock(Name) TimeThroughput(Name, 0);
#define TimeFunction TimeBlock(__func__)
#define ProfilerEndOfCompilationUnit          \
	static_assert(                              \
		__COUNTER__ < Profiler::G.anchors.size(), \
		"Number of profile points exceeds size of Profiler::G.anchors array");

#else// FULL_PROFILER = 0

struct Profiler {
	enum class AutoPrintSummary : bool {
		No = false,
		Yes = true
	};

	explicit Profiler(const AutoPrintSummary should_print_summary_on_destruction = AutoPrintSummary::No) noexcept
			: should_print_summary_on_destruction{should_print_summary_on_destruction},
				start_tsc{read_cpu_timer()},
				cpu_freq{estimate_cpu_timer_frequency()} {}

	~Profiler() {
		if (should_print_summary_on_destruction == AutoPrintSummary::Yes) {
			print_summary();
		}
	}

	void print_summary() {
		end_tsc = read_cpu_timer();
		u64 total_tsc_elapsed = end_tsc - start_tsc;

		std::println(
			"Total time: {:L} clocks = {:L} ns (CPU Frequency = {:L})",
			total_tsc_elapsed,
			nanos(total_tsc_elapsed, cpu_freq),
			cpu_freq);

		should_print_summary_on_destruction = AutoPrintSummary::No;
	}

	void increment_anchor_hits(const gsl::index) noexcept {}

	static Profiler G;
	static gsl::index GParentIndex;

	AutoPrintSummary should_print_summary_on_destruction;
	u64 start_tsc = 0;
	u64 end_tsc = 0;
	u64 cpu_freq = 0;
};

struct ProfileBlock {
	ProfileBlock(std::string_view, const gsl::index, const u64) noexcept {}

	gsl::index get_self_index() const { return 0; }
};

Profiler Profiler::G{AutoPrintSummary::Yes};
gsl::index Profiler::GParentIndex = 0;

#define TimeThroughput(...)
#define TimeBlock(...)
#define TimeFunction
#define ProfilerEndOfCompilationUnit

#endif// FULL_PROFILER

#endif//PROFILER_H
