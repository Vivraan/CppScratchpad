//
// Created by vivraan on 23-11-2023.
//

#ifndef ARGS_H
#define ARGS_H

#include <gsl/gsl>
#include <vector>

class Args {
  std::vector<std::string_view> args_;

public:
  Args(const int argc, gsl::zstring* argv) {
    std::span span{argv, static_cast<std::size_t>(argc)};
    args_.assign(span.begin(), span.end());
  }

  Args(const Args&) = delete;
  Args(Args&&) noexcept = delete;
  auto operator=(const Args&) = delete;
  auto operator=(Args&&) noexcept = delete;
  ~Args() noexcept = default;

  [[nodiscard]] auto begin() const noexcept {
    return args_.begin();
  }

  [[nodiscard]] auto end() const noexcept {
    return args_.end();
  }

  [[nodiscard]] auto size() const noexcept {
    return args_.size();
  }

  [[nodiscard]] std::string_view operator[](const gsl::index i) const noexcept {
    return args_[i];
  }
};

#endif //ARGS_H
