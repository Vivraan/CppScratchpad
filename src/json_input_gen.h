//
// Created by vivraan on 23-11-2023.
//

#ifndef JSON_INPUT_GEN_H
#define JSON_INPUT_GEN_H

#include <cstdint>
#include <random>

#include "args.h"
#include "misc.h"

struct LatLon {
  f64 lat;
  f64 lon;
};

class RandomStateM1P1 {
  std::mt19937_64 gen;
  std::uniform_real_distribution<> udistm1p1{-1, 1};

public:
  explicit RandomStateM1P1(const std::mt19937_64::result_type seed) noexcept
    : gen{seed} {
  }

  RandomStateM1P1() noexcept = delete;
  RandomStateM1P1(const RandomStateM1P1&) noexcept = default;
  RandomStateM1P1(RandomStateM1P1&&) noexcept = default;
  RandomStateM1P1& operator=(const RandomStateM1P1&) noexcept = default;
  RandomStateM1P1& operator=(RandomStateM1P1&&) noexcept = default;
  ~RandomStateM1P1() noexcept = default;

  f64 next() noexcept {
    return udistm1p1(gen);
  }
};

using LatLonRandFunc = LatLon (*)(RandomStateM1P1&) noexcept;

LatLon make_random_latlon_rad(RandomStateM1P1& rs) noexcept;
LatLon make_random_latlon_deg(RandomStateM1P1& rs) noexcept;
constexpr AngleUnit parse_deg_or_rad_pref(const Args& args, AngleUnit default_pref) noexcept;
constexpr u64 parse_u64(std::string_view sv) noexcept;
constexpr u64 parse_num_pairs(const Args& args, u64 default_num_pairs) noexcept;
constexpr u64 parse_rand_seed(const Args& args, u64 default_seed) noexcept;
void generate(const Args& args);

#endif //JSON_INPUT_GEN_H
