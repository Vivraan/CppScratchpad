//
// Created by vivraan on 27-11-2023.
//
#include <execution>
#include <filesystem>
#include <fstream>
#include <gsl/gsl>
#include <numeric>
#include <print>

#include "args.h"
#include "haversine.h"
#include "misc.h"
#include "profiler.h"
#include "rdtsc.h"

namespace {
namespace fs = std::filesystem;

using HvFuncType = decltype(haversine_deg)*;

struct SetupResult {
  HvFuncType haversine;
  fs::path json_path;
  fs::path answers_path;
};

SetupResult setup(const Args& args) {
  TimeFunction;
  SetupResult result;
  const AngleUnit unit_used = args[1].contains("deg") ? AngleUnit::Deg : AngleUnit::Rad;
  result.haversine = unit_used == AngleUnit::Deg ? &haversine_deg : &haversine_rad;
  const fs::path data_root{fs::current_path().parent_path() / "data"};
  result.json_path = data_root / args[1];
  result.answers_path = data_root / args[2];
  if (!exists(result.json_path) || !exists(result.answers_path)) {
    std::println(stderr, R"(One or more files do not exist.
Paths checked:
{}
{})",
      result.json_path.string(), result.answers_path.string());
    std::exit(1);
  }
  return result;
}

std::string read_json(const fs::path& json_path) {
  TimeFunction;
  FILE* json_file = fopen(json_path.string().c_str(), "rb");
  if (!json_file) {
    std::println(stderr, "Failed to open file: {}", json_path.string());
    std::exit(1);
  }
  std::string json;
  fseek(json_file, 0, SEEK_END);
  const auto json_file_size = static_cast<u64>(ftell(json_file));
  fseek(json_file, 0, SEEK_SET);
  TimeThroughput("JSON fread", json_file_size);
  json.resize_and_overwrite(json_file_size, [&](char* p, const usize n) {
    return fread(p, 1, n, json_file);
  });
  fclose(json_file);
  return json;
}

// ReSharper disable once CppParameterMayBeConst -- HvFuncType is a func pointer alias!
f64 hv_bulk_parse_internal(std::string_view json_sv, HvFuncType haversine) {
  // ReSharper disable once CppTooWideScope
  TimeFunction;
  const auto first_newline = json_sv.find_first_of('\n');
  json_sv.remove_prefix(first_newline + 1);
  const auto second_newline = json_sv.find_first_of('\n');
  json_sv.remove_prefix(second_newline + 1);
  const auto num_objs = std::ranges::count(json_sv, '}') - 1;

  using HvArgs = std::array<f64, 4>;
  std::vector<f64> hv_results;
  std::vector<HvArgs> hv_args_vec;
  hv_results.resize(num_objs);
  hv_args_vec.resize(num_objs); //
  {
    // ReSharper disable once CppDFAUnusedValue
    TimeBlock("JSON parse critsec")
    for (gsl::index i = 0;; i++) {
      const auto obj_beg = json_sv.find_first_of('{');
      if (obj_beg == std::string_view::npos) {
        break;
      }
      const auto obj_end = json_sv.find_first_of('}');
      std::string_view obj_view = json_sv.substr(obj_beg + 1, obj_end - obj_beg - 1);
      HvArgs hv_args{};
      for (gsl::index j = 0; j < hv_args.size(); j++) {
        const auto colon_idx = obj_view.find_first_of(':');
        const auto comma_idx = obj_view.find_first_of(',');
        obj_view.remove_prefix(colon_idx + 2);
        auto* num_beg = obj_view.data();
        auto* num_end = obj_view.data() + comma_idx;
        f64 num = 0.0;
        if (auto [p, ec] = std::from_chars(num_beg, num_end, num); ec != std::errc{}) {
          std::println(stderr, "Failed to parse number: {}", obj_view);
          std::exit(1);
        }
        hv_args[j] = num;
      }
      json_sv.remove_prefix(obj_end + 1);
      hv_args_vec[i] = hv_args;
    }
  }
  f64 hv_results_sum = 0.0; //
  {
    TimeThroughput("Haversine Distance Sum Calc", num_objs * sizeof(HvArgs));
    for (const auto& [i, hv_args] : hv_args_vec | std::views::enumerate) {
      const f64 result = haversine(hv_args[0], hv_args[1], hv_args[2], hv_args[3], EARTH_RADIUS_KM);
      hv_results[i] = result;
      hv_results_sum += result;
    }
  }
  return hv_results_sum;
}

f64 read_answers(const fs::path& answers_path) {
  std::vector<f64> hv_answers;
  FILE* answers_file = fopen(answers_path.string().c_str(), "rb");
  if (!answers_file) {
    std::println(stderr, "Failed to open file: {}", answers_path.string());
    std::exit(1);
  }
  fseek(answers_file, 0, SEEK_END);
  const auto answers_file_size = static_cast<u64>(ftell(answers_file));
  fseek(answers_file, 0, SEEK_SET);
  TimeThroughput("Haversine Answers fread", answers_file_size);
  hv_answers.resize(answers_file_size / sizeof(f64));
  fread(hv_answers.data(), sizeof(f64), hv_answers.size(), answers_file);
  fclose(answers_file);
  return std::reduce(std::execution::par_unseq, hv_answers.begin(), hv_answers.end(), 0.0);
}

u64 fib(const u64 n, const ProfileBlock& profile_block) {
  Profiler::G.increment_anchor_hits(profile_block.get_self_index());
  if (n < 2) {
    return n;
  }
  return fib(n - 1, profile_block) + fib(n - 2, profile_block);
}
} // namespace

void hv_bulk_parse(const Args& args) {
  if (args.size() != 3) {
    std::println("Usage: {} <json_file> <answers_file>", args[0]);
    std::exit(0);
  }

  auto [haversine, json_path, answers_path] = setup(args);
  const std::string json = read_json(json_path);
  const f64 hv_results_sum = hv_bulk_parse_internal(json, haversine);
  const f64 hv_answers_sum = read_answers(answers_path);
  std::println("Error: {:.{}Lf}", std::abs(hv_results_sum - hv_answers_sum), std::numeric_limits<f64>::max_digits10);

  constexpr u64 fib_arg = 20;
  f64 fib_v = 0; {
    const ProfileBlock pb{"fib", __COUNTER__ + 1, 0};
    fib_v = fib(fib_arg, pb);
  }

  const u64 empty_profiled_start = read_cpu_timer(); //
  {
    // ReSharper disable once CppDFAUnusedValue
    TimeBlock("[EMPTY BLOCK/Profiler]");
  }
  const u64 empty_profiled_elapsed = read_cpu_timer() - empty_profiled_start;

  std::println("-- Just threw in a fibonacci calls to profile recursion. fib({:L}): {:L}", fib_arg, fib_v);
  std::println("-- Empty block: Profiler={:L} clocks", empty_profiled_elapsed);
}

// ReSharper disable once CppStaticAssertFailure -- CLion freaks out here, but it's fine  ¯\_(ツ)_/¯
ProfilerEndOfCompilationUnit;
