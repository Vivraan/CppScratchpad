//
// Created by vivraan on 27-11-2023.
//

#ifndef HAVERSINE_BULK_PARSE_H
#define HAVERSINE_BULK_PARSE_H

#include "args.h"

void hv_bulk_parse(const Args& args);

#endif //HAVERSINE_BULK_PARSE_H
