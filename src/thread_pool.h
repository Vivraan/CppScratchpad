//
// Created by vivraan on 23-11-2023.
//

#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <condition_variable>
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <stdexcept>
#include <thread>
#include <type_traits>
#include <utility>
#include <vector>

class ThreadPool final {
public:
  explicit ThreadPool(std::size_t threads);
  ~ThreadPool();
  ThreadPool(ThreadPool&&) = delete;
  ThreadPool(const ThreadPool&) = delete;
  ThreadPool& operator=(ThreadPool&&) = delete;
  ThreadPool& operator=(const ThreadPool&) = delete;

  template<typename F, class... Args>
  using enqueue_return_type = std::invoke_result_t<std::decay_t<F>, std::decay_t<Args>...>;

  template<typename F, class... Args>
  auto enqueue(F&& f, Args&&... args) -> std::future<enqueue_return_type<F, Args...>>;

  void wait_for_tasks_done() {
    std::unique_lock lock{queue_mutex};
    waiting = true;
    tasks_done_cv.wait(lock, [this] { return !running && tasks.empty(); });
    waiting = false;
  }

private:
  // need to keep track of threads so we can join them
  std::vector<std::thread> workers;
  // the task queue
  std::queue<std::function<void()>> tasks;

  // synchronization
  std::mutex queue_mutex;
  std::condition_variable tasks_done_cv;
  bool running = false;
  bool waiting = false;
};

// the constructor just launches some amount of workers
inline ThreadPool::ThreadPool(const std::size_t threads) : running{true} {
  for (std::size_t i = 0; i < threads; ++i) {
    workers.emplace_back(
      [this] {
        for (;;) {
          std::unique_lock lock(queue_mutex);
          tasks_done_cv.wait(lock, [this] { return !running || !tasks.empty(); });
          if (!running && tasks.empty()) {
            return;
          }
          auto task = std::move(tasks.front());
          tasks.pop();

          lock.unlock();
          task();
          lock.lock();

          if (waiting && !running && tasks.empty()) {
            tasks_done_cv.notify_all();
          }
        }
      }
    );
  }
}

// the destructor joins all threads
inline ThreadPool::~ThreadPool() {
  // block
  {
    const std::unique_lock lock{queue_mutex};
    running = false;
  }
  tasks_done_cv.notify_all();
  for (std::thread& worker : workers) {
    worker.join();
  }
}

// add new work item to the pool
template<class F, class... Args>
auto ThreadPool::enqueue(
  F&& f, Args&&... args // NOLINT(*-missing-std-forward)
) -> std::future<enqueue_return_type<F, Args...>> {
  using return_type = enqueue_return_type<F, Args...>;

  auto task = std::make_shared<std::packaged_task<return_type()>>(
    [f = std::forward<F>(f), ...args = std::forward<Args>(args)]() mutable {
      return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    });

  std::future<return_type> res = task->get_future();
  // block
  {
    std::unique_lock lock{queue_mutex};

    // don't allow enqueueing after stopping the pool
    // ReSharper disable once CppDFAConstantConditions
    if (!running) {
      throw std::runtime_error("enqueue on stopped ThreadPool");
    }

    tasks.emplace([task] { (*task)(); });
  }
  tasks_done_cv.notify_one();
  return res;
}

#endif //THREAD_POOL_H
