//
// Created by vivraan on 26-11-2023.
//

#ifndef HAVERSINE_H
#define HAVERSINE_H

#include <numbers>

#include "misc.h"

constexpr f64 EARTH_RADIUS_KM = 6372.8;

inline f64 haversine_rad(const f64 p1, const f64 l1, const f64 p2, const f64 l2, const f64 r = EARTH_RADIUS_KM) noexcept {
  const f64 dp = p2 - p1;
  const f64 dl = l2 - l1;
  const f64 a = sq(std::sin(dp / 2)) + std::cos(p1) * std::cos(p2) * sq(std::sin(dl / 2));
  const f64 c = 2 * std::asin(std::sqrt(a));
  return r * c;
}

static f64 to_rad(const f64 x) noexcept {
  return x * std::numbers::pi_v<f64> / 180;
}

inline f64 haversine_deg(const f64 p1, const f64 l1, const f64 p2, const f64 l2, const f64 r = EARTH_RADIUS_KM) noexcept {
  const f64 dp = to_rad(p2 - p1);
  const f64 dl = to_rad(l2 - l1);
  const f64 p1_rad = to_rad(p1);
  const f64 p2_rad = to_rad(p2);
  const f64 t = sq(std::sin(dp / 2)) + std::cos(p1_rad) * std::cos(p2_rad) * sq(std::sin(dl / 2));
  const f64 c = 2 * std::asin(std::sqrt(t));
  return r * c;
}

#endif //HAVERSINE_H
