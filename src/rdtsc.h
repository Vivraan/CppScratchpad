//
// Created by vivraan on 27-11-2023.
//

#ifndef RDTSC_H
#define RDTSC_H

#include <intrin.h>
#include <windows.h>

#include "misc.h"

inline u64 read_cpu_timer() {
  return __rdtsc();
}

inline u64 get_os_timer_frequency() {
  LARGE_INTEGER freq;
  QueryPerformanceFrequency(&freq);
  return static_cast<u64>(freq.QuadPart);
}

inline u64 read_os_timer() {
  LARGE_INTEGER count;
  QueryPerformanceCounter(&count);
  return static_cast<u64>(count.QuadPart);
}

inline u64 estimate_cpu_timer_frequency() {
  const auto freq_os = get_os_timer_frequency();
  const auto start_cpu = read_cpu_timer();
  const auto start_os = read_os_timer();
  auto elapsed_os = 0ULL;
  while (elapsed_os < freq_os * 1e-2) {
    elapsed_os = read_os_timer() - start_os;
  }
  const auto end_cpu = read_cpu_timer();
  const auto elapsed_cpu = end_cpu - start_cpu;
  auto freq_cpu = 0ULL;
  if (elapsed_cpu > 0) {
    freq_cpu = freq_os * elapsed_cpu / elapsed_os;
  }
  return freq_cpu;
}

#endif //RDTSC_H
