#include <locale>
#include <gsl/gsl>

#include "haversine_bulk_parse.h"

int main(int argc, gsl::zstring* argv) {
  std::locale::global(std::locale{"en_GB.UTF-8"});
  hv_bulk_parse({argc, argv});
}
